FROM ubuntu

# Install Java.
RUN apt-get update && apt-get install -q -y \
    curl \
    zip \
    openjdk-11-jdk \
    apt-transport-https \
    gnupg2 \
    ca-certificates \
    maven \
    wget \
    xvfb

# chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list
RUN apt-get update && apt-get install -y google-chrome-stable


# Install gauge
RUN curl -SsL https://downloads.gauge.org/stable | sh

# Install gauge plugins
RUN gauge install java -v 0.7.4
RUN gauge install screenshot
Run gauge install html-report

ENV PATH=$HOME/.gauge:$PATH

RUN gauge -v
RUN Xvfb :99 -screen 0 1920x1080x24 &
RUN export DISPLAY=:99


# Jmeter
# JMeter Version
# ARG JMETER_VERSION="5.4"
# # Download and unpack the JMeter tar file
# RUN cd /opt \
#  && wget https://mirrors.estointernet.in/apache//jmeter/binaries/apache-jmeter-${JMETER_VERSION}.tgz \
#  && tar xzf apache-jmeter-${JMETER_VERSION}.tgz \
#  && rm apache-jmeter-${JMETER_VERSION}.tgz
# # Create a symlink to the jmeter process in a normal bin directory
# RUN ln -s /opt/apache-jmeter-${JMETER_VERSION}/bin/jmeter /usr/local/bin

# docker buildx build --platform linux/amd64 . -t test
# docker build . -t test
# docker run -v $(pwd)/toptal-screening:/gauge -w="/gauge" -v $(pwd)/toptal-screening/reports:/gauge/reports:rw test mvn gauge:execute -DspecsDir=specs/1_E-Commerce
