# Test assignments: ECommerce-01-QA

## 1. Write an automated test for an e-commerce site

### Technology

**BDD Framework:** Gauge (https://gauge.org/)<br>
**Library:** Selenium<br>
**Language:** Java<br>
**Build tool:** Maven<br>

### Running Test

Test result will be generated under ***toptal-screening/reports/html-report*** folder<br>
The report will be in HTML format which is generated from **html-report** plugin from Gauge<br>

```
cd toptal-screening
mvn gauge:execute -DspecsDir=specs/1_E-Commerce
```

### Test cases

Test cases are kept in specs file format and located in<br>
[/toptal-screening/specs/1_E-Commerce](https://git.toptal.com/screening/Natsiree-Futragoon/-/tree/master/toptal-screening/specs/1_E-Commerce)<br>
<br>
It consists of 4 scenarios in 5 different files<br>
Test flows and test cases can be found in following spec files which are written in plain english

**Login**<br>
[01_Login.spec](https://git.toptal.com/screening/Natsiree-Futragoon/-/blob/master/toptal-screening/specs/1_E-Commerce/01_Login.spec)<br>
Main login feature<br>
[01_Login_Invalid.spec](https://git.toptal.com/screening/Natsiree-Futragoon/-/blob/master/toptal-screening/specs/1_E-Commerce/01_Login_Invalid.spec)<br>
Invalid login feature and verify error message<br>
<br>
**Search**<br>
[02_Search.spec](https://git.toptal.com/screening/Natsiree-Futragoon/-/blob/master/toptal-screening/specs/1_E-Commerce/02_Search.spec)<br>
Search feature with 3 criterias<br>
<br>
**Add to cart**<br>
[03_AddToCart.spec](https://git.toptal.com/screening/Natsiree-Futragoon/-/blob/master/toptal-screening/specs/1_E-Commerce/03_AddToCart.spec)<br>
Add to cart feature. Testing add to cart from different entry points and adding same product with same size and different size<br>
<br>
**Checkout**<br>
[04_Checkout.spec](https://git.toptal.com/screening/Natsiree-Futragoon/-/blob/master/toptal-screening/specs/1_E-Commerce/04_Checkout.spec)<br>
Checkout feature. Testing entry points, checkout steps, adding address, shipping T&C and payment type<br>



## 2. Write an automated test for a REST API service

### Technology

**BDD Framework:** Gauge (https://gauge.org/)<br>
**Library:** rest-assured<br>
**Language:** Java<br>
**Build tool:** Maven<br>

### Running Test

```
cd toptal-screening
mvn gauge:execute -DspecsDir=specs/2_GoogleMapsAPI
```

### Test cases

This test are using the same framework as task 1 above but test cases are kept in different folder<br>
[/toptal-screening/specs/2_GoogleMapsAPI](https://git.toptal.com/screening/Natsiree-Futragoon/-/tree/master/toptal-screening/specs/2_GoogleMapsAPI)<br>
<br>
This test will verify Geocode API from Google Maps by checking both valid and invalid cases as follows<br>
- Query Geocode API with correct address and API key
- Query Geocode API with wrong API key 
- Query Geocode API without API key
- Query Geocode API with wrong parameters
- Query Geocode API that return no result


## 3. Load test

This load test will simulate 1000 users to visit www.facebook.com in a period of 15s<br>

### Technology

JMeter<br>

### Running Test

Bash script is created to help running jmeter cli to fire the load and generating html report<br>
This command will trigger the test in ***HomePage.jmx*** and html report can be found in ***report-output*** folder after the test finish

```
cd toptal-screening/jmeter
sh run-homepage.sh
```

### Test cases

The test are located in [/toptal-screening/jmeter](https://git.toptal.com/screening/Natsiree-Futragoon/-/tree/master/toptal-screening/jmeter) folder<br>
and divided into 2 parts

**1. Normal load**<br>
This thread group will fire a small load to the application which is 10 users in 15s to measure the performance in a normal condition as a baseline<br>

**2. Load test**<br>
This thread group will fire 1000 users to the application in 15s and the result will be compared to the normal load result<br>

Load test result and analysis can be found in this [wiki page](https://git.toptal.com/screening/Natsiree-Futragoon/-/wikis/Load-Test-Result)

## Short project video

1. Automated test for an e-commerce site<br>
https://git.toptal.com/screening/Natsiree-Futragoon/-/blob/master/videos/1_E-Commerce.mov

2. Automated test for a REST API service<br>
https://git.toptal.com/screening/Natsiree-Futragoon/-/blob/master/videos/2_GoogleMapsAPI.mov

3. Load Test<br>
https://git.toptal.com/screening/Natsiree-Futragoon/-/blob/master/videos/3_LoadTest.mov


## CI/CD

Gitlab CI/CD yaml (.gitlab-ci.yml) is also added into this project.
There are 2 stages of this pipeline which are 1. validate and 2. test.

**validate**
This stage will be triggered every time a new merge request is created.
This will call gauge:validate command to validate the correctness of test files under specs folder.
If any steps implementation are duplicated or missing, this stage will fail.

**test**
Thi stage will be triggered on new commit to 'master' branch.
It will run both ui and api test


## Bugs report

Bugs found during the test are reported under ***Issues*** menu in this project
