echo "Cleaning old reoports"
rm -rf report-output
rm report.csv

echo "Running JMeter script"
jmeter -n -t HomePage.jmx -l result.csv

echo "Generating Report"
jmeter -g result.csv -o report-output
