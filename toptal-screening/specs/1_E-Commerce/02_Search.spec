Search Feature
=================

Search from Search bar
--------------------
tags: failed, issues-3
* Given I am on Home Page
* When I search "Dress" from search bar
* Then I should see correct search result

Search with Enter key
--------------------
* Given I am on Home Page
* When I search "shirt" from search bar with Enter key
* Then I should see correct search result

Search shouldn't be case-sensitive
--------------------
* Given I am on Home Page
* When I search "SHIRT" from search bar
* Then I should see correct search result

Search with no result should not break the page
--------------------
* Given I am on Home Page
* When I search "Nothing" from search bar
* Then I should see No search results correctly

Search bar autocomplete
-------------------
* Given I am on Home Page
* When I enter search text "Printed" in search bar
* Then I should see correct auto complete result

Size Filter
------------------
* Given I am on Home Page
* And I navigate to Women section
* When I filter product by Size S
* Then I should see product filter with size S correctly

Color Filter
-----------------
* Given I am on Home Page
* And I navigate to Women section
* When I filter product by Color White
* Then I should see product filter with white color correctly

Product Type Filter
-----------------
* Given I am on Home Page
* And I navigate to Women section
* When I filter Tops products
* Then I should see product filter Tops correctly

Criteria filter (> 1 filters)
------------------
* Given I am on Home Page
* And I navigate to Women section
* When I filter product by Size S
* And I filter product by Color White
* And I filter Tops products
* Then I should see product filter with size S correctly
* And I should see product filter with white color correctly
* And I should see product filter Tops correctly
