Login Invalid Test
===================

This spec is to test invalid cases of Login Feature
It will input invalid credentails and verify that user can't login and page show correct error message


|email|password|error|
|------|------|------|
|natsiree.f@gmail.com|abcde|Authentication failed|
|natsiree.x@gmail.com|12345|Authentication failed|
|natsiree.f@gmail.com||Password is required|
|natsiree.f@gmail.com|123|Invalid password|
||12345|An email address required|
|||An email address required|


Login with invalid data
---------------
* Given I am on Sign In Page
* When I login with email and password <email> <password>
* Then I should still be on the log in page
* And I should see login error <error>
