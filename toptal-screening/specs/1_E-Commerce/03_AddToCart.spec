Add Products to Cart
===================

Add to Cart from Home Page
--------------------
* Given I am on Home Page
* When I add product name "Blouse" to cart from Home Page
* Then I should see the item added to cart in popup correctly
* And I should see the item added to cart with QTY = "1"


Add to Cart from Quick View
---------------------
* Given I am on Home Page
* When I add product name "Blouse" QTY = "2" to cart from Quick View
* Then I should see the item added to cart with QTY = "2"


Add Same Product to Cart
---------------------
* Given I am on Home Page
* When I add product name "Blouse" to cart from Home Page
* Then I should see the item added to cart in popup correctly
* And I should see the item added to cart with QTY = "1"

// Add same product again
* Given I am on Home Page
* When I add product name "Blouse" to cart from Home Page
* Then I should see the item added to cart in popup correctly
* And I should see the item added to cart with QTY = "2"


Add Same Product to Cart - different size
---------------------
* Given I am on Home Page
* When I add product name "Blouse" to cart from Home Page
* Then I should see the item added to cart in popup correctly

* Given I am on Home Page
* When I add product name "Blouse" size = "M" to cart from Quick View

* Then I should see "2" products added to cart
