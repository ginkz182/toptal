Login Feature
================

This spec is to test Login Feature only
Not include other features on the same screen like Forgot Password or Create an Account.

Pre-requisite: User already registered and have valid account

Verify Login Page
-----------------
* Given I am on Sign In Page
* Then I should see Sign In Page layout correctly


Login with valid credentials and Logout
---------------
* Given I am on Sign In Page
* When I login with email and password "natsiree.f@gmail.com" "12345"
* Then I should be on My Account page
* And I should be logged in successfully with name "Natsiree F"

* When I Sign out from the site
* Then I should be logged out successfully
