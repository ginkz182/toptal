Checkout Process
================

Checkout without Login Lead to Authentication Page
-----------------
* Given I am on Home Page
* And I add product name "Blouse" QTY = "1" to cart from Quick View
* When I click Proceed to checkout button
* Then I should be redirected to Sign In page


Checkout From Cart Summary Page
------------------
* Given I already logged in to the website
* And I have saved address
* And I navigate to Home Page
* And I add product name "Blouse" QTY = "1" to cart from Quick View
* When I click Proceed to checkout button
* Then I should be in Address Step in checkout


Checkout From Cart Popup
--------------------
* Given I already logged in to the website
* And I have saved address
* And I have product in cart
* When I checkout from Cart Popup
* Then I should be in Cart Summary Page


Checkout with Save Address
---------------------
* Given I already logged in to the website
* And I have saved address
* And I have product in cart
* When I checkout from Cart Popup
* And I proceed all checkout steps
* Then I should see order created successfully


Cannot Checkout Without Agreeing to T&C
------------------------
* Given I already logged in to the website
* And I have saved address
* And I have product in cart
* And I checkout from Cart Popup
* And I click Proceed to checkout button
* And I click Proceed to checkout button on Your Addresses page
* When I click Proceed on Shipping page without agree Terms
* Then I should not be allowed to proceed checkout
* And I am still on Shipping step


Checkout and Pay by Bank Wire
------------------------
* Given I already logged in to the website
* And I have saved address
* And I have product in cart
* And I checkout from Cart Popup
* And I click Proceed to checkout button
* And I click Proceed to checkout button on Your Addresses page
* And I click Proceed to checkout button on Shipping page

* When I select Pay by bank wire
* Then I should see correct payment selected

* When I click Confirm Order
* Then I should see order confirmation correctly


Checkout and Pay by Check
------------------------
* Given I already logged in to the website
* And I have saved address
* And I have product in cart
* And I checkout from Cart Popup
* And I click Proceed to checkout button
* And I click Proceed to checkout button on Your Addresses page
* And I click Proceed to checkout button on Shipping page

* When I select Pay by check
* Then I should see correct payment selected

* When I click Confirm Order
* Then I should see order confirmation correctly


Checkout with No Saved Address Should Lead to Add Address
-----------------
* Given I already logged in to the website
* And I don't have any saved address
* And I have product in cart
* When I checkout from Cart Popup
* And I click Proceed to checkout button
* Then I should see screen to add address

* When I add new address in checkout step
* Then I should be in Address Step in checkout
