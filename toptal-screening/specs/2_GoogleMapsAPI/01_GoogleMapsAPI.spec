Google Maps API
=================

tags: api

Query Geocode API output format Json
-----------------
* Query Geocode by address "Nirvana" should return geographic data
* Query Geocode by address "Nirvana Beyond Lite" should return geographic data
* Query Geocode by address "Nirvana+Beyond+Lite" should return geographic data


Query Geocode API output format XML
-----------------
* Query Geocode by address "Nirvana" in XML format should return geographic data
* Query Geocode by address "Nirvana Beyond Lite" in XML format should return geographic data
* Query Geocode by address "Nirvana+Beyond+Lite" in XML format should return geographic data

Query Geocode API with wrong API Key
------------------
* Query Geocode by address "Nirvana" with wrong API key should return error

Query Geocode API without API key
---------------------
* Query Geocode by address "Nirvana" without API key should return error

Query Geocode API with Wrong Parameters
----------------------
* Query Geocode by address "Nirvana" with wrong params should return error

No result from Geocode API
----------------------
* Query Geocode by address "Nirvana%2032%20newtyok" with no result should not fail
