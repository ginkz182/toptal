package steps;

import com.thoughtworks.gauge.Step;

import pages.MyAccountPage;
import pages.MyAddressesPage;

public class MyAddressesSteps {
    MyAddressesPage myAddressesPage = new MyAddressesPage();
    MyAccountPage myAccountPage = new MyAccountPage();

    @Step("And I don't have any saved address")
    public void ensureNoAddress() {
        myAccountPage.goToMyAddressesPage();
        myAddressesPage.deleteAllAddresses();
    }

    @Step("And I delete all existing addresses")
    public void deleteAllAddresses() {
        myAddressesPage.deleteAllAddresses();
    }
    
    @Step("And I have saved address")
    public void addAddress() {
        myAccountPage.goToMyAddressesPage();
        myAddressesPage.addAddressIfNotExist();
    }
}
