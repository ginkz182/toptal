package steps;

import com.thoughtworks.gauge.Step;

import pages.HomePage;

public class HomepageSteps {

    HomePage homePage = new HomePage();
    @Step({"Given I am on Home Page", 
        "And I navigate to Home Page"})
    public void navigateToHomePage() {
        homePage.navigateToHomePage();
    }

    @Step("And I navigate to Women section")
    public void navigateToWomen() {
        homePage.goToWomenSection();
    }
}
