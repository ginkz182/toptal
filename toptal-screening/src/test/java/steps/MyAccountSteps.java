package steps;

import com.thoughtworks.gauge.Step;

import pages.MyAccountPage;

public class MyAccountSteps {
    
    MyAccountPage myAccountPage= new MyAccountPage();
    
    @Step("And I navigate to My Addresses Page")
    public void navigateToMyAddresses() {
        myAccountPage.goToMyAddressesPage();
    }

}
