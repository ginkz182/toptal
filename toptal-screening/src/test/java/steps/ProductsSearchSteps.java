package steps;

import com.thoughtworks.gauge.Step;

import pages.ProductsSearchPage;

public class ProductsSearchSteps {
    
    ProductsSearchPage productsSearchPage = new ProductsSearchPage();

    @Step("When I filter product by Size S")
    public void filterSizeS() {
        productsSearchPage.filterBySizeS();
    }

    @Step({"When I filter product by Color White", "And I filter product by Color White"})
    public void filterWhite() {
        productsSearchPage.filterByWhiteColor();
    }

    @Step({"When I filter Tops products", "And I filter Tops products"})
    public void filterTops() {
        productsSearchPage.filterByTops();
    }

    @Step("Then I should see product filter with size S correctly")
    public void verifySizeSFilter() {
        productsSearchPage.waitProductLoad();
        productsSearchPage.verifySizeSFilter();
    }

    @Step({"Then I should see product filter with white color correctly", 
        "And I should see product filter with white color correctly"})
    public void verifyWhite() {
        productsSearchPage.waitProductLoad();
        productsSearchPage.verifyColorWhiteFilter();
    }

    @Step({"Then I should see product filter Tops correctly",
        "And I should see product filter Tops correctly"})
    public void verifyTops() {
        productsSearchPage.waitProductLoad();
        productsSearchPage.verifyTops();
    }
}
