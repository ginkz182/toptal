package steps;

import com.thoughtworks.gauge.Step;

import pages.CartModal;
import pages.HomePage;
import pages.ProductCard;


public class AddToCartSteps {

    ProductCard productCard = new ProductCard();
    CartModal cartModal = new CartModal();
    HomePage homePage = new HomePage();

    String addedProduct;

    @Step({"When I add product name <productName> to cart from Home Page",
        "And I add product name <productName> to cart from Home Page"})
    public void addProductToCart(String productName) {
        addedProduct = productName;
        productCard.hoverProductCart(productName);
        productCard.clickAddToCart();
    }

    @Step({"When I add product name <productName> QTY = <quantity> to cart from Quick View",
        "And I add product name <productName> QTY = <quantity> to cart from Quick View"})
    public void addProductToCartQuickViewQTY(String productName, String quantity) {
        addedProduct = productName;
        productCard.hoverProductCart(productName);
        productCard.clickHoveredQuickView();
        productCard.switchToQuickviewIframe();
        productCard.selectQuantityQuickView(quantity);
        productCard.clickAddToCartQuickView();
    }

    @Step("When I add product name <productName> size = <size> to cart from Quick View")
    public void addProductToCartQuickViewSize(String productName, String size) {
        addedProduct = productName;
        productCard.hoverProductCart(productName);
        productCard.clickHoveredQuickView();
        productCard.switchToQuickviewIframe();
        productCard.selectSizeQuickView(size);
        productCard.clickAddToCartQuickView();
    }

    @Step("And I have product in cart")
    public void addSomeProductToCart() {
        homePage.navigateToHomePage();
        addProductToCart(System.getenv("DEFAULT_PRODUCT"));
        cartModal.closeCartPopup();
    }

    @Step("Then I should see the item added to cart in popup correctly")
    public void verifyCartPopup() {
        cartModal.verifyProductAddToCartPage(addedProduct);
        cartModal.closeCartPopup();
    }

    @Step({"Then I should see the item added to cart with QTY = <quantity>",
        "And I should see the item added to cart with QTY = <quantity>"})
    public void verifyProductInCart(String quantity) {
        homePage.hoverOnShoppingCart();
        homePage.verifyProductNameOnShoppingCart(addedProduct);
        homePage.verifyProductQuantityOnShoppingCart(quantity);
    }

    @Step("Then I should see <num> products added to cart")
    public void verifyNumberOfProductInCart(int num) {
        homePage.hoverOnShoppingCart();
        homePage.verifyNumberOfProductInCart(num);
    }

    @Step({"When I checkout from Cart Popup",
        "And I checkout from Cart Popup"})
    public void checkoutFromCartPopup() {
        homePage.hoverOnShoppingCart();
        homePage.clickCheckoutFromCartPopup();

    }
 
}
