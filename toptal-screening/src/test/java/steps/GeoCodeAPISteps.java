package steps;

import com.thoughtworks.gauge.Step;

import api.GeoCodeAPI;
import io.restassured.response.Response;

public class GeoCodeAPISteps {
    
    GeoCodeAPI geoCodeAPI = new GeoCodeAPI();
    Response response;

    @Step("Query Geocode by address <address> should return geographic data")
    public void geocode(String address) {
        response = geoCodeAPI.getGeocodeFromAddress(address, System.getenv("GOOGLE_MAPS_API_KEY"));
        geoCodeAPI.verifyOKResponse(response);
    }

    @Step("Query Geocode by address <address> in XML format should return geographic data")
    public void geocodeXML(String address) {
        response = geoCodeAPI.getGeocodeFromAddress(address, System.getenv("GOOGLE_MAPS_API_KEY"), "xml");
        geoCodeAPI.verifyOKResponseXML(response);
    }

    @Step("Query Geocode by address <address> with wrong API key should return error")
    public void geocodeWrongAPIKey(String address) {
        response = geoCodeAPI.getGeocodeFromAddress(address, "WRONG_KEY");
        geoCodeAPI.verifyWrongAPIKey(response);
    }

    @Step("Query Geocode by address <address> without API key should return error")
    public void geocodeNoAPIKey(String address) {
        response = geoCodeAPI.getGeocodeFromAddressNoAPI(address);
        geoCodeAPI.verifyRequestDenied(response);
    }

    @Step("Query Geocode by address <address> with wrong params should return error")
    public void geocodeWrongParams(String address) {
        response = geoCodeAPI.getGeocodeFromAddressWrongParam(address, System.getenv("GOOGLE_MAPS_API_KEY"));
        geoCodeAPI.verifyMissingParam(response);
    }

    @Step("Query Geocode by address <address> with no result should not fail")
    public void geocodeNoReult(String address) {
        response = geoCodeAPI.getGeocodeFromAddress(address, System.getenv("GOOGLE_MAPS_API_KEY"));
        geoCodeAPI.verifyZeroResult(response);
    }
}
