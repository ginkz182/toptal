package steps;
import com.thoughtworks.gauge.Step;

import pages.LoginPage;
import pages.MyAccountPage;

public class LoginSteps {

    LoginPage loginPage = new LoginPage();
    MyAccountPage myAccountPage = new MyAccountPage();

    @Step("Given I am on Sign In Page")
    public void navigateToSignInPage() {
        loginPage.navigateToSignInPage();
    }

    @Step({"Then I should see Sign In Page layout correctly",
        "Then I should be logged out successfully",
        "Then I should still be on the log in page",
        "Then I should be redirected to Sign In page"})
    public void verifySignInPage() {
        loginPage.verifyPage();
        loginPage.verifyPageElements();
    }

    @Step("When I login with email and password <email> <password>")
    public void login(String email, String password) {
        loginPage.loginWith(email, password);
    }

    @Step("Given I already logged in to the website")
    public void login() {
        loginPage.navigateToSignInPage();
        loginPage.loginWith(System.getenv("EMAIL"), System.getenv("PASSWORD"));
    }

    @Step("Then I should be on My Account page")
    public void verifyUserOnMyAccount() {
        myAccountPage.verifyPage();
    }

    @Step("And I should be logged in successfully with name <name>")
    public void verifyUserLoggedin(String name) {
        loginPage.verifyLoggedin(name);
    }

    @Step("When I Sign out from the site")
    public void signout() {
        loginPage.signOut();
    }

    @Step("And I should see login error <error>")
    public void verifyError(String error) {
        loginPage.verifyErrorMessage(error);
    }
}
