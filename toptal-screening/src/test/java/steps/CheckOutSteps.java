package steps;

import com.thoughtworks.gauge.Step;

import pages.AddressCheckoutPage;
import pages.CartSummaryPage;
import pages.PaymentCheckoutPage;
import pages.ShippingCheckoutPage;
import pages.YourAddressesPage;

public class CheckOutSteps {
    
    YourAddressesPage yourAddressesPage = new YourAddressesPage();
    AddressCheckoutPage addressCheckoutPage = new AddressCheckoutPage();
    CartSummaryPage cartSummaryPage = new CartSummaryPage();
    ShippingCheckoutPage shippingCheckoutPage = new ShippingCheckoutPage();
    PaymentCheckoutPage paymentCheckoutPage = new PaymentCheckoutPage();

    @Step("Then I should see screen to add address")
    public void verifyAddAddressDuringCheckout() {
        yourAddressesPage.verifyPage();
    }

    @Step("When I add new address in checkout step")
    public void addNewAddress() {
        yourAddressesPage.addAddress();
    }

    @Step("Then I should be in Address Step in checkout")
    public void verifyAddressStep() {
        addressCheckoutPage.verifyPage();
        addressCheckoutPage.verifyAddressCheckout();
    }

    @Step({"When I click Proceed to checkout button",
        "And I click Proceed to checkout button"})
    public void summaryStepCheckout() {
        cartSummaryPage.clickProceedToCheckoutButton();
    }

    @Step("Then I should be in Cart Summary Page")
    public void verifyCartSummaryPage() {
        cartSummaryPage.verifyCartSummaryPage();
    }

    @Step("And I click Proceed to checkout button on Your Addresses page")
    public void addressStepCheckout() {
        addressCheckoutPage.clickProceedToCheckout();
    }

    @Step("And I click Proceed to checkout button on Shipping page")
    public void shippingStepCheckout() {
        shippingCheckoutPage.agreeTerms();
        shippingCheckoutPage.clickProceedToCheckout();
    }

    @Step("When I click Proceed on Shipping page without agree Terms")
    public void shippingStepCheckoutWithoutTC() {
        shippingCheckoutPage.clickProceedToCheckout();
    }

    @Step("When I select Pay by check")
    public void payByCheck() {
        paymentCheckoutPage.payByCheck();
    }

    @Step("When I select Pay by bank wire")
    public void payByBankWire() {
        paymentCheckoutPage.payByBankWire();
    }

    @Step("Then I should see correct payment selected")
    public void verifySelectedPayment() {
        paymentCheckoutPage.verifySelectedPayment();
    }

    @Step("When I click Confirm Order")
    public void confirmOrder() {
        paymentCheckoutPage.clickConfirmOrder();
    }

    @Step("Then I should see order confirmation correctly")
    public void verifyOrderConfirmation() {
        paymentCheckoutPage.verifyOderCompleted();
    }

    @Step("And I proceed all checkout steps")
    public void proceedCheckoutSteps() {
        summaryStepCheckout();
        addressStepCheckout();
        shippingStepCheckout();
        payByBankWire();
        confirmOrder();
    }

    @Step("Then I should see order created successfully")
    public void verifyOrderSuccess() {
        paymentCheckoutPage.verifyOderCompleted();
    }

    @Step("Then I should not be allowed to proceed checkout")
    public void verifyTCPopup() {
        shippingCheckoutPage.verifyTCPopup();
    }

    @Step("And I am still on Shipping step")
    public void verifyShippingStep() {
        shippingCheckoutPage.closeErrorPopup();
        shippingCheckoutPage.verifyShippingPage();
    }
}
