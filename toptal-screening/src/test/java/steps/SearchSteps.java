package steps;

import com.thoughtworks.gauge.Step;

import org.openqa.selenium.Keys;

import pages.SearchComponent;

public class SearchSteps {

    String searchText = "";
    SearchComponent searchComponent = new SearchComponent();

    @Step("When I enter search text <text> in search bar")
    public void enterSearchText(String text) {
        searchText = text;
        searchComponent.inputSearchText(text);
    }

    @Step("When I search <text> from search bar")
    public void searchBar(String text) {
        enterSearchText(text);
        searchComponent.clickSearch();
    }

    @Step("When I search <text> from search bar with Enter key")
    public void searchBarWithEnter(String text) {
        searchText = text;
        searchComponent.inputSearchText(text + Keys.ENTER);
    }

    @Step("Then I should see correct search result")
    public void verifySearchResult() {
        searchComponent.verifySearchResultContains(searchText);
    }

    @Step("Then I should see No search results correctly")
    public void verifyNoSearchResult() {
        searchComponent.verifyNoSearchResult(searchText);
    }

    @Step("Then I should see correct auto complete result")
    public void verifyAutoComplete() {
        searchComponent.verifyAutoCompleteResult(searchText);
    }

    
}
