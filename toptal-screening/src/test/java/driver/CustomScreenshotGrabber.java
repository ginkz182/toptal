package driver;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

import com.thoughtworks.gauge.screenshot.CustomScreenshotWriter;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class CustomScreenshotGrabber implements CustomScreenshotWriter {

    // Return a screenshot file name
    @Override
    public String takeScreenshot() {
        if (DriverFactory.getInstance().getDriver() == null) return null;

        TakesScreenshot driver = (TakesScreenshot) DriverFactory.getInstance().getDriver();
        String screenshotFileName = String.format("screenshot-%s.png", UUID.randomUUID().toString());
        try {
            Files.write(Path.of(System.getenv("gauge_screenshots_dir"), screenshotFileName),
                    driver.getScreenshotAs(OutputType.BYTES));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return screenshotFileName;
    }

}
