package pages;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.support.PageFactory;

public class YourAddressesPage extends MyAddressesPage {

    public YourAddressesPage() {
        PageFactory.initElements(getDriver(), this);
        PAGE_NAME = "Your addresses";
    }

    public void verifyPage() {
        SoftAssertions soft = new SoftAssertions();
        soft.assertThat(breadcrumb.getText().contains(PAGE_NAME));
        soft.assertAll();
    }

}
