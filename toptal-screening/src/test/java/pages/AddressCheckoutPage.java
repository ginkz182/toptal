package pages;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddressCheckoutPage extends BasePage {
    
    @FindBy(id = "id_address_delivery")
    WebElement deliveryAddressDropdown;

    @FindBy(id = "address_delivery")
    WebElement deliveryAddressBox;

    @FindBy(id = "address_invoice")
    WebElement billingAddressBox;

    @FindBy(css = "button[name='processAddress']")
    WebElement proceedToCheckoutBtn;

    public AddressCheckoutPage() {
        PageFactory.initElements(getDriver(), this);
    }

    public void verifyAddressCheckout() {
        SoftAssertions soft = new SoftAssertions();
        soft.assertThat(deliveryAddressDropdown.isDisplayed());
        soft.assertThat(deliveryAddressBox.isDisplayed());
        soft.assertThat(billingAddressBox.isDisplayed());
        soft.assertAll();
    }

    public void clickProceedToCheckout() {
        proceedToCheckoutBtn.click();
    }
}
