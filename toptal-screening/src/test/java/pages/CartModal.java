package pages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CartModal extends BasePage {
    
    @FindBy(css = "div.layer_cart_product")
    WebElement cartPopupLayer;

    @FindBy(css = "div.layer_cart_product span.title")
    WebElement cartPopupTitle;

    @FindBy(id = "layer_cart_product_title")
    WebElement cartPopupProductTitle;

    @FindBy(css = "#layer_cart span.cross")
    WebElement closeCartPopupBtn;

    public CartModal() {
        PageFactory.initElements(getDriver(), this);
    }

    public void verifyProductAddToCartPage(String productName) {
        waitElementVisible(cartPopupLayer);
        assertTrue("Cart Popup is not showing", 
            cartPopupTitle.getText().contains("Product successfully added to your shopping cart"));
        assertEquals(productName, cartPopupProductTitle.getText());
    }

    public void closeCartPopup() {
        waitElementVisible(cartPopupLayer);
        closeCartPopupBtn.click();
    }
}
