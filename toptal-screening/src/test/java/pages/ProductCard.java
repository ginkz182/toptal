package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ProductCard extends BasePage {
    
    @FindBy(css = "div.button-container[style]:not([style='display: none;'])")
    WebElement addToCartFromHover;

    @FindBy(css = "li.hovered a.quick-view")
    WebElement hoveredQuickView;

    @FindBy(css = "div.fancybox-overlay iframe")
    WebElement quickviewIframe;

    @FindBy(id = "quantity_wanted")
    WebElement quickviewQtyInput;

    @FindBy(id = "group_1")
    WebElement quickviewSizeDropdown;

    @FindBy(css = "#add_to_cart button")
    WebElement quickviewAddToCart;

    public ProductCard() {
        PageFactory.initElements(getDriver(), this);
    }

    public void hoverProductCart(String productName) {
        WebElement product = getDriver().findElement(By.cssSelector("div.product-container a[title='" + productName + "']"));
        scrollToElement(product);
        hoverElement(product);
    }

    public void clickAddToCart() {
        addToCartFromHover.click();
    }

    public void clickHoveredQuickView() {
        hoveredQuickView.click();
    }

    public void switchToQuickviewIframe() {
        waitElementVisible(quickviewIframe);
        getDriver().switchTo().frame(quickviewIframe);
    }

    public void selectQuantityQuickView(String quantity) {
        quickviewQtyInput.clear();
        quickviewQtyInput.sendKeys(quantity);
    }

    public void selectSizeQuickView(String size) {
        Select sizeSelect = new Select(quickviewSizeDropdown);
        sizeSelect.selectByVisibleText(size);
    
    }

    public void clickAddToCartQuickView() {
        quickviewAddToCart.click();
        getDriver().switchTo().parentFrame();
    }

}
