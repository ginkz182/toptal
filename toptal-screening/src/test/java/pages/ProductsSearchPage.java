package pages;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductsSearchPage extends BasePage {
    
    @FindBy(id = "layered_id_attribute_group_1")
    WebElement sizeSCheckBox;

    @FindBy(id = "layered_id_attribute_group_8")
    WebElement colorWhiteCheckBox;

    @FindBy(id = "layered_category_4")
    WebElement topsCheckBox;

    @FindBy(id = "enabled_filters")
    WebElement enabledFilters;

    @FindBy(id = "layered_block_left")
    WebElement filterDiv;

    By loadedProductsSelector = By.cssSelector("ul#product_list[style='opacity: 1;']");
    By loadingIconSelector = By.cssSelector("img[src*=loader.gif]");

    public ProductsSearchPage() {
        PageFactory.initElements(getDriver(), this);
    }

    public void filterBySizeS() {
        waitElementVisible(filterDiv);
        if (!sizeSCheckBox.isSelected()) {
            sizeSCheckBox.click();
        }
    }

    public void filterByWhiteColor() {
        waitElementVisible(filterDiv);
        if (!colorWhiteCheckBox.isSelected()) {
            colorWhiteCheckBox.click();
        }
    }

    public void filterByTops() {
        waitElementVisible(filterDiv);
        if (!topsCheckBox.isSelected()) {
            topsCheckBox.click();
        }
    }

    public void verifySizeSFilter() {
        verifySlug("size-s");
        verifyEnabledFilters("Size: S");
    }

    public void verifyColorWhiteFilter() {
        verifySlug("color-white");
        verifyEnabledFilters("Color: White");
    }

    public void verifyTops() {
        verifySlug("categories-tops");
        verifyEnabledFilters("Categories: Tops");
    }

    public void waitProductLoad() {
        waitUntilElementNotPresenceBy(loadingIconSelector);
        waitUntilElementPresenceBy(loadedProductsSelector);
    }

    public void verifyEnabledFilters(String filter) {
        assertTrue(filter + " should be in enabled filters. Actual: " + enabledFilters.getText(),
        enabledFilters.getText().contains(filter));
    }
    
}
