package pages;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CartSummaryPage extends BasePage {
    @FindBy(css = "a.standard-checkout")
    WebElement proceedToCheckoutBtn;

    @FindBy(id = "cart_summary")
    WebElement cartSummaryTable;

    @FindBy(css = "li.step_current")
    WebElement activeStep;

    public CartSummaryPage() {
        PageFactory.initElements(getDriver(), this);
    }

    public void verifyCartSummaryPage() {
        assertTrue("Active Step should be at Summary step",
            activeStep.getText().contains("Summary"));
        assertTrue("Cart Summary Table is not displayed", 
            cartSummaryTable.isDisplayed());
    }

    public void clickProceedToCheckoutButton() {
        proceedToCheckoutBtn.click();
    }
}
