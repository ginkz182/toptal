package pages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShippingCheckoutPage extends BasePage {
    @FindBy(css = "button[name='processCarrier']")
    WebElement proceedToCheckoutBtn;

    @FindBy(id = "cgv")
    WebElement tcCheckbox;

    @FindBy(css = "p.fancybox-error")
    WebElement errorPopup;

    @FindBy(css = "a.fancybox-close")
    WebElement closeErrorIcon;

    @FindBy(css = "li.step_current")
    WebElement activeStep;

    @FindBy(css = "div.delivery_options_address")
    WebElement deliveryOption;

    public ShippingCheckoutPage() {
        PageFactory.initElements(getDriver(), this);
    }

    public void clickProceedToCheckout() {
        proceedToCheckoutBtn.click();
    }

    public void agreeTerms() {
        if (!tcCheckbox.isSelected()) {
            tcCheckbox.click();
        }
    }

    public void verifyTCPopup() {
        waitElementVisible(errorPopup);
        assertEquals("You must agree to the terms of service before continuing.",
            errorPopup.getText());
    }

    public void closeErrorPopup() {
        waitElementVisible(closeErrorIcon);
        closeErrorIcon.click();
    }

    public void verifyShippingPage() {
        assertTrue("Active Step should be at Shipping step",
            activeStep.getText().contains("Shipping"));
        assertTrue("Delivery Option is not displayed", 
            deliveryOption.isDisplayed());
    }
}
