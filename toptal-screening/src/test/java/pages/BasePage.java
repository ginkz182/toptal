package pages;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;

import static org.junit.Assert.assertTrue;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import driver.DriverFactory;

public class BasePage {
    String PAGE_NAME = "";

    @FindBy(css = "a.login")
    WebElement signInLink;

    @FindBy(css = "a.logout")
    WebElement signOutLink;

    @FindBy(css = "a.account")
    WebElement accountName;

    @FindBy(css = "ul.sf-menu a[title='Women']")
    WebElement womenLink;

    @FindBy(css = "ul.sf-menu a[title='Dresses']")
    WebElement dressesLink;

    @FindBy(css = "ul.sf-menu a[title='T-Shirts']")
    WebElement tshirtsLink;

    @FindBy(css = "div.breadcrumb")
    WebElement breadcrumb;

    @FindBy(css = "h1.page-heading")
    WebElement pageTitle;

    WebDriverWait wait = new WebDriverWait(getDriver(), 15);

    public WebDriver getDriver() {
        return DriverFactory.getInstance().getDriver();
    }    

    public BasePage() {
        PageFactory.initElements(getDriver(), this);
    }

    public void navigateToHomePage() {
        getDriver().get(System.getenv("APP_URL"));
    }

    public void verifyPage() {
        waitElementVisible(breadcrumb);
        SoftAssertions soft = new SoftAssertions();
        soft.assertThat(breadcrumb.getText().contains(PAGE_NAME));
        soft.assertThat(pageTitle.getText().equals(PAGE_NAME));
        soft.assertAll();
    }

    public boolean isElementExist(WebElement element) {
        try {
            element.getText();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public void navigateToSignInPage() {
        navigateToHomePage();
        signInLink.click();
    }

    public void signOut() {
        signOutLink.click();
    }

    public void goToWomenSection() {
        womenLink.click();
    }

    public void waitUntilElementNotPresenceBy(By by) {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    public void waitUntilElementPresenceBy(By by) {
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public void waitElementVisible(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void verifySlug(String slug) {
        assertTrue("Page url should contain '" + slug + "' slug. " + 
            "Acutal: " + getDriver().getCurrentUrl(), 
            getDriver().getCurrentUrl().contains(slug));
    }

    protected void hoverElement(WebElement element) {
        Actions action = new Actions(getDriver());
        action.moveToElement(element).perform();
    }

    public void scrollToElement(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].scrollIntoView(true);", element);
    }

    protected void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
