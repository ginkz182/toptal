package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccountPage extends BasePage {

    @FindBy(css = "a[title='Addresses']")
    WebElement myAddressesLink;

    public MyAccountPage() {
        PageFactory.initElements(getDriver(), this);
        PAGE_NAME = "My Account";
    }
    
    public void goToMyAddressesPage() {
        accountName.click();
        myAddressesLink.click();
    }
}
