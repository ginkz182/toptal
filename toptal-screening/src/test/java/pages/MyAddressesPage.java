package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class MyAddressesPage extends BasePage {
    
    @FindBy(css = "a[title='Add an address']")
    WebElement addAddressBtn;

    @FindBy(id = "address1")
    WebElement address1Input;

    @FindBy(id = "city")
    WebElement cityInput;   

    @FindBy(id = "id_state")
    WebElement stateDropdown;

    @FindBy(id = "postcode")
    WebElement postcodeInput;

    @FindBy(id = "phone")
    WebElement phoneInput;

    @FindBy(id = "phone_mobile")
    WebElement mobileInput;

    @FindBy(id = "alias")
    WebElement addressAliasInput;

    @FindBy(id = "submitAddress")
    WebElement saveBtn;

    @FindBy(id = "address_delivery")
    WebElement deliveryAddressBox;

    public MyAddressesPage() {
        PageFactory.initElements(getDriver(), this);
    }

    public void clickAddAddress() {
        addAddressBtn.click();
    }

    public void deleteAllAddresses() {
        List<WebElement> addressesList = getDriver().findElements(By.cssSelector("div.address ul"));
        while (addressesList.size() > 0) {
            addressesList.get(0).findElement(By.cssSelector("a[title='Delete']")).click();
            getDriver().switchTo().alert().accept();
            addressesList = getDriver().findElements(By.cssSelector("div.address ul"));
        }
    }

    public void addAddress() {
        address1Input.sendKeys("11/11");
        cityInput.sendKeys("New York");
        Select stateSelect = new Select(stateDropdown);
        stateSelect.selectByVisibleText("Alaska");
        postcodeInput.sendKeys("11111");
        phoneInput.sendKeys("1111111");
        mobileInput.sendKeys("1111111");
        addressAliasInput.sendKeys("Address " + System.currentTimeMillis()); //put random number
        saveBtn.click();
    }

    public void addAddressIfNotExist() {
        if (getDriver().findElements(By.cssSelector("div.address")).size() == 0) {
            clickAddAddress();
            addAddress();
        }
    }
}
