package pages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {

    @FindBy(id = "email_create")
    WebElement createEmailInput;

    @FindBy(id = "SubmitCreate")
    WebElement createAccountBtn;

    @FindBy(id = "email")
    WebElement emailInput;

    @FindBy(id = "passwd")
    WebElement passwordInput;

    @FindBy(id = "SubmitLogin")
    WebElement loginBtn;

    @FindBy(css = "#center_column > div.alert")
    WebElement errorDiv;

    public LoginPage() {
        PageFactory.initElements(getDriver(), this);
        PAGE_NAME = "Authentication";
    }

    public void verifyPageElements() {
        SoftAssertions soft = new SoftAssertions();
        soft.assertThat(!isElementExist(signOutLink));
        soft.assertThat(signInLink.isDisplayed());

        soft.assertThat(createEmailInput.isDisplayed());
        soft.assertThat(createAccountBtn.isDisplayed());

        soft.assertThat(emailInput.isDisplayed());
        soft.assertThat(passwordInput.isDisplayed());
        soft.assertThat(loginBtn.isDisplayed());
        soft.assertAll();
    }

    public void enterEmail(String email) {
        emailInput.clear();
        emailInput.sendKeys(email);
    }

    public void enterPassword(String password) {
        passwordInput.clear();
        passwordInput.sendKeys(password);
    }

    public void clickLoginBtn() {
        loginBtn.click();
    }

    public void loginWith(String email, String password) {
        waitElementVisible(emailInput);
        enterEmail(email);
        enterPassword(password);
        clickLoginBtn();
    }

    public void verifyLoggedin(String name) {
        assertTrue("Sign Out link should be existed", signOutLink.isDisplayed());
        assertTrue("Sign In link should not existed", !isElementExist(signInLink));
        assertEquals("Account name should be showing as expected", name, accountName.getText());
    }

    public void verifyErrorMessage(String error) {
        assertTrue("Error message should be showing as expected. Expected: " + error +
            " Actual:" + errorDiv.getText(), errorDiv.getText().contains(error));
    }
}
