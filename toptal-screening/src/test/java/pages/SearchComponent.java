package pages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchComponent extends HomePage {

    @FindBy(id = "search_query_top")
    WebElement searchBar;

    @FindBy(css = "button[name='submit_search']")
    WebElement searchButton;

    @FindBy(css = "div.ac_results")
    WebElement autoCompleteResults;

    @FindBy(css = "#center_column p.alert")
    WebElement searchResultAlert;

    @FindBy(css ="div.product-container")
    List<WebElement> searchResultProducts;

    public SearchComponent() {
        PageFactory.initElements(getDriver(), this);
    }

    public void inputSearchText(String searchText) {
        searchBar.clear();
        searchBar.sendKeys(searchText);
    }

    public void clickSearch() {
        searchButton.click();
    }

    public void waitForAutoCompleteResult() {
        waitElementVisible(autoCompleteResults);
    }

    public void verifyAutoCompleteResult(String searchText) {
        waitForAutoCompleteResult();
        List<WebElement> results = autoCompleteResults.findElements(By.tagName("li"));

        for (WebElement result: results) {
            assertTrue("Auto complete result doesn't contain search text (" + searchText + "). Actual: " + result.getText(), 
                result.getText().toLowerCase().contains(searchText.toLowerCase()));
        }
    }

    public void verifyNoSearchResult(String searchText) {
        waitElementVisible(searchResultAlert);
        assertEquals("No results were found for your search \"" + searchText + "\"", searchResultAlert.getText().trim());
    }

    public void verifySearchResultContains(String searchText) {
        for (WebElement result: searchResultProducts) {
            String productName = result.findElement(By.cssSelector("h5[itemprop='name']")).getText();
            assertTrue("Search Result doesn't contains search text (" + searchText + "). Actual: " + productName,
                productName.toLowerCase().contains(searchText.toLowerCase()));
        }
    }
}
