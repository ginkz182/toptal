package pages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaymentCheckoutPage extends BasePage {
    @FindBy(css = "a.bankwire")
    WebElement payBankWire;

    @FindBy(css = "a.cheque")
    WebElement payCheck;

    @FindBy(css = "#cart_navigation button")
    WebElement confirmOrderBtn;

    @FindBy(css = "h3.page-subheading")
    WebElement paymentTypeH3;

    @FindBy(css = "p.alert-success")
    WebElement successBar;

    @FindBy(css = "div.box")
    WebElement paymentDetail;

    String paymentType = "";

    public PaymentCheckoutPage() {
        PageFactory.initElements(getDriver(), this);
    }

    public void payByBankWire() {
        paymentType = "BANK-WIRE PAYMENT";
        payBankWire.click();
    }

    public void payByCheck() {
        paymentType = "CHECK PAYMENT";
        payCheck.click();
    }

    public void clickConfirmOrder() {
        confirmOrderBtn.click();
    }

    public void verifySelectedPayment() {
        assertEquals("Payment Type is not as selected",
            paymentType, paymentTypeH3.getText());
    }

    public void verifyOderCompleted() {
        assertTrue(successBar.isDisplayed());
        assertEquals("Your order on My Store is complete.",
            successBar.getText());
        
            if (paymentType.equals("Check payment")) {
                assertEquals("Payment details not correct",
                    "Your check must include:", paymentTypeH3.getText());
            } else if (paymentType.equals("Bank-wire payment")) {
                assertTrue("Payment details not correct",
                    paymentDetail.getText().contains("Please send us a bank wire with"));               
            }

    }
}
