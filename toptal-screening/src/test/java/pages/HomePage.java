package pages;

import static org.junit.Assert.assertEquals;

import java.time.Duration;
import java.util.List;

import org.apache.commons.lang3.ThreadUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {
    
    @FindBy(css = "div.shopping_cart a")
    WebElement shoppingCartDiv;

    @FindBy(css = "div.cart_block a.cart_block_product_name")
    WebElement shoppingCartProductName;

    @FindBy(css = "div.cart_block span.quantity")
    WebElement shoppingCartProductQuantity;

    @FindBy(css = "dt[data-id*='cart_block_product']")
    List<WebElement> shoppingCartProducts;

    @FindBy(id = "button_order_cart")
    WebElement checkoutCartBtn;

    public HomePage() {
        PageFactory.initElements(getDriver(), this);
    }

    public void hoverOnShoppingCart() {
        hoverElement(shoppingCartDiv);
        //wait popup animated
        sleep(1000);
    }

    public void verifyProductNameOnShoppingCart(String productName) {
        assertEquals("Product name in cart is not as expected",
            productName, shoppingCartProductName.getText());
    }

    public void verifyProductQuantityOnShoppingCart(String quantity) {
        assertEquals("Product quantity in cart is not as expected",
            quantity, shoppingCartProductQuantity.getText());
    }

    public void verifyNumberOfProductInCart(int num) {
        assertEquals("Number of item in cart is not as expected",
            num, shoppingCartProducts.size());
    }

    public void clickCheckoutFromCartPopup() {
        checkoutCartBtn.click();
    }
}
