package api;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class GeoCodeAPI {
    
    String baseURL = "https://maps.googleapis.com/maps/api/geocode/";
    String defaultOutput = "json";

    String missing_param_err = "Invalid request. Missing the 'address', 'components', 'latlng' or 'place_id' parameter.";
    String apikey_err = "You must use an API key to authenticate each request to Google Maps Platform APIs. For additional information, please refer to http://g.co/dev/maps-no-account";
    String wrong_apikey_err = "The provided API key is invalid.";

    String REQUEST_DENIED = "REQUEST_DENIED";
    String INVALID_REQUEST = "INVALID_REQUEST";
    String ZERO_RESULT = "ZERO_RESULTS";

    private Response getData(String address, String apiKey, String output) {
        ContentType contentType = output.equals("xml")? ContentType.XML : ContentType.JSON;

        return given().contentType(contentType)
            // .log().all()
                .param("address", address)
                .param("key", apiKey)
            .when()
                .get(baseURL + output);
    }
    public Response getGeocodeFromAddress(String address, String apiKey, String output) {
        return getData(address, apiKey, output);
    }

    public Response getGeocodeFromAddress(String address, String apiKey) {
        return getData(address, apiKey, defaultOutput);
    }

    public Response getGeocodeFromAddressNoAPI(String address) {
        return given()
            .param("address", address)
        .when()
            .get(baseURL + defaultOutput);
    }

    public Response getGeocodeFromAddressWrongParam(String address, String apiKey) {
        return given()
                // .log().all()
                    .param("a", address)
                    .param("key", apiKey)
                .when()
                    .get(baseURL + defaultOutput);
    }

    public void verifyOKResponse(Response response) {
        response.then()
        // .log().all()
            .statusCode(200)
            .body("status", equalTo("OK"))
            .body("results[0]", hasKey("address_components"))
            .body("results[0]", hasKey("formatted_address"))
            .body("results[0]", hasKey("geometry"))
            .body("results[0].geometry", hasKey("location"))
            .body("results[0].geometry.location", hasKey("lat"))
            .body("results[0].geometry.location", hasKey("lng"))
            .body("results[0]", hasKey("place_id"))
            .body("results[0]", hasKey("types"));
    }

    public void verifyOKResponseXML(Response response) {
        response.then()
        // .log().all()
            .statusCode(200)
            .body("GeocodeResponse.status", equalTo("OK"))
            .body(hasXPath("//GeocodeResponse/result/address_component"))
            .body(hasXPath("//GeocodeResponse/result/formatted_address"))
            .body(hasXPath("//GeocodeResponse/result/geometry"))
            .body(hasXPath("//GeocodeResponse/result/geometry/location"))
            .body(hasXPath("//GeocodeResponse/result/geometry/location/lat"))
            .body(hasXPath("//GeocodeResponse/result/geometry/location/lng"))
            .body(hasXPath("//GeocodeResponse/result/place_id"));
    }

    public void verifyRequestDenied(Response response) {
        response.then()
        // .log().all()
            .statusCode(200)
            .body("status", equalTo(REQUEST_DENIED))
            .body("error_message", equalTo(apikey_err));
    }

    public void verifyWrongAPIKey(Response response) {
        response.then()
        // .log().all()
            .statusCode(200)
            .body("status", equalTo(REQUEST_DENIED))
            .body("error_message", equalTo(wrong_apikey_err));
    }
    
    public void verifyMissingParam(Response response) {
        response.then()
        // .log().all()
            .statusCode(400)
            .body("status", equalTo(INVALID_REQUEST))
            .body("error_message", equalTo(missing_param_err));    
    }

    public void verifyZeroResult(Response response) {
        response.then()
            .statusCode(200)
            .body("status", equalTo(ZERO_RESULT));
    }

}
